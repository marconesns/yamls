# Example: Deploying WordPress and MySQL with Persistent Volumes

Aplicação de teste [Wordpress + Mysql + PVC](https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/)

Este tutorial mostra como implantar um site WordPress e um banco de dados MySQL usando o Minikube. Ambos os aplicativos usam PersistentVolumes e PersistentVolumeClaims para armazenar dados.

Um PersistentVolume (PV) é uma parte do armazenamento no cluster que foi provisionado manualmente por um administrador ou provisionado dinamicamente pelo Kubernetes usando um StorageClass . Um PersistentVolumeClaim (PVC) é uma solicitação de armazenamento por um usuário que pode ser atendida por um PV. PersistentVolumes e PersistentVolumeClaims são independentes dos ciclos de vida do pod e preservam os dados reiniciando, reagendando e até mesmo excluindo pods.

```
 kubectl apply -k ./
```

```
 minikube service wordpress --url
```