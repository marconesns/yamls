# Create secret

> Criar de forma imperativa

```shell
kubectl create secret generic mysql-secret \
  --from-literal root_password=mypassroot \
  --from-literal user=myuser \
  --from-literal password=mypass \
  --from-literal database=mydb
```

> Criar de forma declarativa

```shell
echo -n 'mypassroot' | base64
echo -n 'myuser' | base64
echo -n 'mypass' | base64
echo -n 'mydb' | base64
```

> vim mysql-secret.yml

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: mysql-secret
data:
  root_password: bXlwYXNzcm9vdA==
  user: bXl1c2Vy
  password: bXlwYXNz
  database: bXlkYg==
```

```shell
kubectl set env deployment/mysql-deploy --prefix MYSQL_ --from secret/mysql-secret
```

```shell
kubectl port-forward deployment/mysql-deploy 13306:3306
```

```shell
mysql -uroot -h 127.0.0.1 -P 13306 -pmypassroot mydb < 07-confimap-secrets/mysql_dump.sql
```
