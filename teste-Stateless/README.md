# Example: Deploying PHP Guestbook application with Redis


Aplicação de teste [Gest book](https://kubernetes.io/docs/tutorials/stateless-application/guestbook/)


Este tutorial mostra como criar e implantar um aplicativo da Web multicamada simples (não pronto para produção) usando Kubernetes e Docker . Este exemplo consiste nos seguintes componentes:

Um Redis de instância única para armazenar entradas do livro de visitas
Várias instâncias de front-end da Web

## Objetivos
* Inicie um líder Redis.
* Inicie dois seguidores Redis.
* Inicie o front-end do livro de visitas.
* Exponha e visualize o serviço de front-end.
* Limpar.



```shell
kubectl port-forward svc/frontend 8080:80
```

load the page http://localhost:8080 in your browser to view your guestbook.

# Para remover

kubectl delete deployment -l app=redis
kubectl delete service -l app=redis
kubectl delete deployment frontend
kubectl delete service frontend